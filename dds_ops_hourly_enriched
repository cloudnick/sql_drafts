create table public.dds_mart_ops_hourly_enriched
            (dim_terminalid, dim_organizationid, dim_businessline, dim_gateway, dim_is_bpa, dim_auth_currency_code,
             dim_auth_currency_nominal, dim_auth_currency_rate, dim_auth_currency_rate_date, dim_date_hour,
             v_refundscount, v_paymentscount, v_declinescount, v_cardpayoutcompletedcount, v_cardpayoutdeclinedcount,
             v_auth_cur_paymentsamount, v_auth_cur_refundsamount, v_auth_cur_cardpayoutcompletedamount,
             v_auth_cur_declinesamount, v_auth_currency_cardpayoutdeclinedamount, v_auth_cur_bankfee,
             v_auth_cur_bankdeclinesfee, v_auth_cur_bankservicefee, v_auth_cur_processorfee,
             v_auth_cur_processordeclinesfee, v_auth_cur_processorservicefee, v_auth_cur_partnerfee,
             v_auth_cur_marketplacepartnerfee, v_set_cur_paymentsamount, v_set_cur_refundsamount,
             v_set_cur_cardpayoutcompletedamount, v_set_cur_declinesamount, v_set_cur_bankfee,
             v_set_cur_bankdeclinesfee, v_set_cur_bankservicefe, v_set_cur_processorfee, v_set_cur_processordeclinesfee,
             v_set_cur_processorservicefee, v_set_cur_partnerfee, v_set_cur_marketplacepartnerfee, v_rub_paymentsamount,
             v_rub_refundsamount, v_rub_cardpayoutcompletedamount, v_rub_declinesamount, v_rub_cardpayoutdeclinedamount,
             v_rub_bankfee, v_rub_bankdeclinesfee, v_rub_bankservicefee, v_rub_processorfee, v_rub_processordeclinesfee,
             v_rub_processorservicefee, v_rub_partnerfee, v_rub_marketplacepartnerfee)
WITH (
	appendonly=true,
	compresstype=quicklz,
	orientation=column
)
as
WITH dt AS (
    SELECT sh.id_terminalid                                          AS dim_terminalid,
           g.last_ver_organizationid                                 AS dim_organizationid,
           g.last_ver_businessline                                   AS dim_businessline,
           sh.id_gateway                                             AS dim_gateway,
           CASE
               WHEN g.bpa_start_date <= sh.id_date THEN 1
               ELSE 0
               END                                                   AS dim_is_bpa,
           dc.code                                                   AS dim_auth_currency_code,
           CASE
               WHEN dc.code = 'RUB'::text THEN 1::numeric
               ELSE mdc.nominal
               END                                                   AS dim_auth_currency_nominal,
           CASE
               WHEN dc.code = 'RUB'::text THEN 1::numeric
               ELSE mdc.value
               END                                                   AS dim_auth_currency_rate,
           mdc.date                                                  AS dim_auth_currency_rate_date,
           sh.id_date                                                AS dim_date_hour,
           sh.refundscount                                           AS v_refundscount,
           sh.paymentscount                                          AS v_paymentscount,
           sh.declinescount                                          AS v_declinescount,
           sh.cardpayoutcompletedcount                               AS v_cardpayoutcompletedcount,
           sh.cardpayoutdeclinedcount                                AS v_cardpayoutdeclinedcount,
           sh.paymentsamount / 100000::numeric                       AS v_auth_cur_paymentsamount,
           sh.refundsamount / 100000::numeric                        AS v_auth_cur_refundsamount,
           sh.cardpayoutcompletedamount / 100000::numeric            AS v_auth_cur_cardpayoutcompletedamount,
           sh.declinesamount / 100000::numeric                       AS v_auth_cur_declinesamount,
           sh.cardpayoutdeclinedamount / 100000::numeric             AS v_auth_currency_cardpayoutdeclinedamount,
           sh.bankfee / 100000::numeric                              AS v_auth_cur_bankfee,
           sh.bankdeclinesfee / 100000::numeric                      AS v_auth_cur_bankdeclinesfee,
           sh.bankservicefee / 100000::numeric                       AS v_auth_cur_bankservicefee,
           sh.processorfee / 100000::numeric                         AS v_auth_cur_processorfee,
           sh.processordeclinesfee / 100000::numeric                 AS v_auth_cur_processordeclinesfee,
           sh.processorservicefee / 100000::numeric                  AS v_auth_cur_processorservicefee,
           sh.partnerfee / 100000::numeric                           AS v_auth_cur_partnerfee,
           sh.marketplacepartnerfee / 100000::numeric                AS v_auth_cur_marketplacepartnerfee,
           sh.paymentsamount_settlement / 100000::numeric            AS v_set_cur_paymentsamount,
           sh.refundsamount_settlement / 100000::numeric             AS v_set_cur_refundsamount,
           sh.cardpayoutcompletedamount_settlement / 100000::numeric AS v_set_cur_cardpayoutcompletedamount,
           sh.declinesamount_settlement / 100000::numeric            AS v_set_cur_declinesamount,
           sh.bankfee_settlement / 100000::numeric                   AS v_set_cur_bankfee,
           sh.bankdeclinesfee_settlement / 100000::numeric           AS v_set_cur_bankdeclinesfee,
           sh.bankservicefee_settlement / 100000::numeric            AS v_set_cur_bankservicefe,
           sh.processorfee_settlement / 100000::numeric              AS v_set_cur_processorfee,
           sh.processordeclinesfee_settlement / 100000::numeric      AS v_set_cur_processordeclinesfee,
           sh.processorservicefee_settlement / 100000::numeric       AS v_set_cur_processorservicefee,
           sh.partnerfee_settlement / 100000::numeric                AS v_set_cur_partnerfee,
           sh.marketplacepartnerfee_settlement / 100000::numeric     AS v_set_cur_marketplacepartnerfee,
           date_trunc('month', sh.id_date + INTERVAL '3 hour')::date AS report_date
    FROM usr_wrk.stc_hourly sh
             LEFT JOIN public.dds_ops_terminal_gwts g ON g.gateway = sh.id_gateway AND g.terminalid = sh.id_terminalid
             LEFT JOIN usr_wrk.dict_currency dc ON dc.id = sh.id_currency
             LEFT JOIN usr_wrk.mssql_directory_currencies mdc ON dc.code = mdc.code AND mdc.date =
                                                                                        (date_trunc('month'::text,
                                                                                                    sh.id_date +
                                                                                                    '01:00:00'::interval *
                                                                                                    3::double precision +
                                                                                                    '1 mon'::interval *
                                                                                                    1::double precision) -
                                                                                         '1 day'::interval *
                                                                                         1::double precision -
                                                                                         '01:00:00'::interval *
                                                                                         3::double precision)
)
SELECT dt.dim_terminalid,
       dt.dim_organizationid,
       dt.dim_businessline,
       dt.dim_gateway,
       dt.dim_is_bpa,
       dt.dim_auth_currency_code,
       dt.dim_auth_currency_nominal,
       dt.dim_auth_currency_rate,
       dt.dim_auth_currency_rate_date,
       dt.dim_date_hour,
       dt.v_refundscount,
       dt.v_paymentscount,
       dt.v_declinescount,
       dt.v_cardpayoutcompletedcount,
       dt.v_cardpayoutdeclinedcount,
       dt.v_auth_cur_paymentsamount,
       dt.v_auth_cur_refundsamount,
       dt.v_auth_cur_cardpayoutcompletedamount,
       dt.v_auth_cur_declinesamount,
       dt.v_auth_currency_cardpayoutdeclinedamount,
       dt.v_auth_cur_bankfee,
       dt.v_auth_cur_bankdeclinesfee,
       dt.v_auth_cur_bankservicefee,
       dt.v_auth_cur_processorfee,
       dt.v_auth_cur_processordeclinesfee,
       dt.v_auth_cur_processorservicefee,
       dt.v_auth_cur_partnerfee,
       dt.v_auth_cur_marketplacepartnerfee,
       dt.v_set_cur_paymentsamount,
       dt.v_set_cur_refundsamount,
       dt.v_set_cur_cardpayoutcompletedamount,
       dt.v_set_cur_declinesamount,
       dt.v_set_cur_bankfee,
       dt.v_set_cur_bankdeclinesfee,
       dt.v_set_cur_bankservicefe,
       dt.v_set_cur_processorfee,
       dt.v_set_cur_processordeclinesfee,
       dt.v_set_cur_processorservicefee,
       dt.v_set_cur_partnerfee,
       dt.v_set_cur_marketplacepartnerfee,
       dt.v_auth_cur_paymentsamount / dt.dim_auth_currency_nominal * dt.dim_auth_currency_rate                AS v_rub_paymentsamount,
       dt.v_auth_cur_refundsamount / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_refundsamount,
       dt.v_auth_cur_cardpayoutcompletedamount / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_cardpayoutcompletedamount,
       dt.v_auth_cur_declinesamount / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_declinesamount,
       dt.v_auth_currency_cardpayoutdeclinedamount / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_cardpayoutdeclinedamount,
       dt.v_auth_cur_bankfee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_bankfee,
       dt.v_auth_cur_bankdeclinesfee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_bankdeclinesfee,
       dt.v_auth_cur_bankservicefee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_bankservicefee,
       dt.v_auth_cur_processorfee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_processorfee,
       dt.v_auth_cur_processordeclinesfee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_processordeclinesfee,
       dt.v_auth_cur_processorservicefee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_processorservicefee,
       dt.v_auth_cur_partnerfee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_partnerfee,
       dt.v_auth_cur_marketplacepartnerfee / dt.dim_auth_currency_nominal *
       dt.dim_auth_currency_rate                                                                              AS v_rub_marketplacepartnerfee,
       dt.report_date                                                                                         as report_date
FROM dt
;

grant select on public.dds_mart_ops_hourly_enriched to "cp.data.reader";

grant delete, insert, references, select, trigger, truncate, update on public.dds_mart_ops_hourly_enriched to "cp.tech_etl";

alter table public.dds_mart_ops_hourly_enriched
set
    distributed by (report_date);