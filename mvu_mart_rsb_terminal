create materialized view mvu_mart_rsb_terminal as
WITH cte AS (
    SELECT dg.id_region,
           st.type,
           st.status,
           st.confirmdate,
           st.currency,
           st.terminalid,
           st.gateway,
           st.scopeid,
           ms.name,
           gg.orgid,
           st.mid,
           st.entrymode,
           st.terminalmode,
           st.cardtype,
           st.issuerbankname,
           st.applepay,
           st.androidpay,
           st.amount / 100000::numeric       AS amount,
           st.processorfee / 100000::numeric AS processor,
           st.currency,
           dc.code                           AS curr
    FROM stc_transactiondata_backup20211110 st
             LEFT JOIN usr_wrk.dict_gateway dg ON dg.id::numeric = st.gateway
             LEFT JOIN usr_wrk.gwt_gatewayterminalcommission gg
                       ON gg.terminalid::numeric = st.terminalid AND gg.islastversion = 1
             LEFT JOIN usr_wrk.dict_currency dc ON dc.id::numeric = st.currency
             LEFT JOIN usr_wrk.mssql_merchant_scopes ms ON ms.id::numeric = st.scopeid
    WHERE st.status = 3::numeric
      AND st.type = 0::numeric
      AND st.gateway = 21::numeric
      AND st.currency <> 0::numeric
      AND st.confirmdate > '2020-11-30 21:00:00'::timestamp without time zone
      AND st.bankoperationdate >= '2020-11-01 21:00:00'::timestamp without time zone
    GROUP BY dg.id_region, st.type, st.status, st.confirmdate, st.currency, st.terminalid, st.gateway, st.scopeid,
             ms.name, st.mid, gg.orgid, st.entrymode, st.terminalmode, st.cardtype, st.issuerbankname, st.applepay,
             st.androidpay, dc.code, st.amount, st.processorfee
)
SELECT cte.terminalid,
       cte.scopeid,
       mmo.inn,
       mms.name                                  AS scopename,
       a.name                                    AS m1,
       a1.name                                   AS rop1,
       b.name                                    AS m2,
       b1.name                                   AS rop2,
       c.name                                    AS m3,
       c1.name                                   AS rop3,
       cte.curr,
       sum(cte.amount)                           AS amount,
       sum(cte.processor)                        AS processorfee,
       date_part('month'::text, cte.confirmdate) AS months,
       date_part('year'::text, cte.confirmdate)  AS years,
       now()::date                               AS date_actual
FROM cte cte(id_region, type, status, confirmdate, currency, terminalid, gateway, scopeid, name, mid, entrymode, orgid,
             terminalmode, cardtype, issuerbankname, applepay, androidpay, amount, processor, currency_1, curr)
         LEFT JOIN usr_wrk.mssql_merchant_scopes mms ON cte.scopeid = mms.id::numeric
         LEFT JOIN usr_wrk.mssql_merchant_terminals mmt ON mmt.id::numeric = cte.terminalid
         LEFT JOIN usr_wrk.mssql_manager_useraccounts a ON a.id = mmt.managerid
         LEFT JOIN usr_wrk.mssql_manager_useraccounts b ON b.id = mmt.secondmanagerid
         LEFT JOIN usr_wrk.mssql_manager_useraccounts c ON c.id = mmt.thirdmanagerid
         LEFT JOIN usr_wrk.mssql_manager_useraccounts a1 ON a.supervisorid = a1.id
         LEFT JOIN usr_wrk.mssql_manager_useraccounts b1 ON b.supervisorid = b1.id
         LEFT JOIN usr_wrk.mssql_manager_useraccounts c1 ON c.supervisorid = c1.id
         LEFT JOIN usr_wrk.mssql_merchant_organizations mmo ON mmo.id::numeric = cte.orgid
GROUP BY cte.terminalid, cte.scopeid, mmo.inn, mms.name, a.name, b.name, c.name, a1.name, b1.name, c1.name,
         date_part('month'::text, cte.confirmdate), date_part('year'::text, cte.confirmdate), cte.curr;

alter materialized view mvu_mart_rsb_terminal owner to "cp.nandrianova";

grant delete, insert, references, select, trigger, truncate, update on mvu_mart_rsb_terminal to nifi;

grant select on mvu_mart_rsb_terminal to "b.zhitnikov_int";

grant select on mvu_mart_rsb_terminal to "cp.lmiroshnichenko";

grant select on mvu_mart_rsb_terminal to "cp.vbochkova";

grant select on mvu_mart_rsb_terminal to "cp.asannikov";

grant select on mvu_mart_rsb_terminal to "cp.data.reader";

grant delete, insert, references, select, trigger, truncate, update on mvu_mart_rsb_terminal to zeppelin;

grant delete, insert, references, select, trigger, truncate, update on mvu_mart_rsb_terminal to "cp.tech_etl";

